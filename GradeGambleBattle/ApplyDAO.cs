﻿using System;
using System.Collections.Generic;
using System.Data; // DataTable
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GradeGambleBattle
{
    class ApplyDAO
    {
        private DBConnector dbConnector;

        public ApplyDAO()
        {
            this.dbConnector = new DBConnector();
        }

        public void addApply(int stdID, int sbjID)
        {
            string query = "insert into apply (`stdID`, `sbjID`, `middleScore`, `finalScore`, `grade`) ";
            query += "values ('" + stdID + "', '" + sbjID + "', -1, -1, -1)";
            dbConnector.selectQuery(query);
        }

        public void delApply(int stdID, int sbjID)
        {
            string query = "delete from apply where stdID = '" + stdID + "' and sbjID = '" + sbjID + "'";
            dbConnector.selectQuery(query);
        }

        public void setMiddleScore(int stdID, int sbjID, int score)
        {
            string query = "update apply set middleScore = '" + score + "' where stdID = '" + stdID + "' and sbjID = '" + sbjID + "'";
            dbConnector.selectQuery(query);
        }

        public void setFinalScore(int stdID, int sbjID, int score)
        {
            string query = "update apply set finalScore = '" + score + "' where stdID = '" + stdID + "' and sbjID = '" + sbjID + "'";
            dbConnector.selectQuery(query);
        }

        public void setGrade(int stdID, int sbjID, double grade)
        {
            string query = "update apply set grade = '" + grade + "' where stdID = '" + stdID + "' and sbjID = '" + sbjID + "'";
            dbConnector.selectQuery(query);
        }
    }
}
