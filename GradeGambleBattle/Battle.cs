﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GradeGambleBattle
{

    abstract public class Battle
    {
        private List<Student> student;
        private List<Student> accept;
        private int token;
        private bool end;

        private BattleDAO dao;

        public Battle(List<Student> student, List<Student> accept, int token, bool end)
        {
            this.student = student;
            this.accept = accept;
            this.token = token;
            this.end = end;

            this.dao = new BattleDAO();
        }

        public void addBattle()
        {
            //student.Add(user);
            // Database
        }

        public void delBattle()
        {
            //student.Remove(user);
            // Database
        }

        public void endBattle()
        {
            end = false;

            // Database
        }

        public void joinBattle(Student user)
        {
            accept.Add(user);
            user.addToken(token * -1);
        }

        public void rejectBattle(Student user)
        {
            accept.Remove(user);
        }

        abstract public void allocate();

        public List<Student> getStudent()
        {
            return this.student;
        }

        public List<Student> getAccept()
        {
            return this.accept;
        }

        public int getToken()
        {
            return this.token;
        }

        public bool getEnd()
        {
            return this.end;
        }
    }

    public class PersonalBattle : Battle
    {
        public PersonalBattle(List<Student> student, List<Student> accept, int token, bool end) : base(student, accept, token, end) { }

        public override void allocate()
        {
            double[] stdGrade = new double[2];

            stdGrade[0] = getAccept().ElementAt(0).getGrade();
            stdGrade[1] = getAccept().ElementAt(1).getGrade();

            if (stdGrade[0] > stdGrade[1])
            {
                getAccept().ElementAt(0).addToken(getToken() * 2);
            }
            else if (stdGrade[0] < stdGrade[1])
            {
                getAccept().ElementAt(1).addToken(getToken() * 2);
            }
            else
            {
                getAccept().ElementAt(0).addToken(getToken());
                getAccept().ElementAt(0).addToken(getToken());
            }

            endBattle();
        }
    }

    public class GroupBattle : Battle
    {
        public GroupBattle(List<Student> student, List<Student> accept, int token, bool end) : base(student, accept, token, end) { }

        public override void allocate()
        {
            Student[] st = sortGrade();

            st[0].addToken((int)(getToken() * getAccept().Count * 0.5));
            st[1].addToken((int)(getToken() * getAccept().Count * 0.3));
            st[2].addToken((int)(getToken() * getAccept().Count * 0.2));
        }

        public Student[] sortGrade()
        {
            int i, j;
            double max;

            List<Student> accept = getAccept();
            Student[] st = new Student[3];

            for (i = 0; i <= 2; i++)
            {
                max = -1;

                for (j = 0; j < getAccept().Count; j++)
                {
                    double grade = getAccept().ElementAt(j).getGrade();

                    if (max < grade)
                    {
                        max = grade;
                        st[i] = getAccept().ElementAt(j);
                    }

                    getAccept().Remove(st[i]);
                }
            }

            return st;
        }
    }
}
