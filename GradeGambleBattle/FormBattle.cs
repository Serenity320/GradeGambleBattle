﻿using System;
using System.Collections; // IEnumerator
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GradeGambleBattle
{
    public partial class FormBattle : Form
    {
        private FormMain frmMain;
        private FormGamble frmGamble;

        public FormBattle(FormMain frmMain, FormGamble frmGamble)
        {
            InitializeComponent();

            this.frmMain = frmMain;
            this.frmGamble = frmGamble;

            // ListView
            lvwStuent.View = View.Details;
            lvwStuent.FullRowSelect = true;
            lvwStuent.GridLines = true;

            lvwStuent.Columns.Add("학번");
            lvwStuent.Columns.Add("이름");

            // TextBox
            txtToken.Text = "1";
        }

        private void btnSuccess_Click(object sender, EventArgs e)
        {
            if (lvwStuent.Items.Count > 0)
            { 
                Battle battle;
                List<Student> battleStudent = new List<Student>();

                battleStudent.Add(frmMain.getGame().getUser());

                foreach (ListViewItem row in this.lvwStuent.Items)
                {
                    int studentID = Convert.ToInt32(row.SubItems[0].Text);

                    List<Student> student = frmMain.getGame().getStudent();

                    for (IEnumerator j = student.GetEnumerator(); j.MoveNext(); )
                    {
                        Student tmpStudent = (Student)j.Current;

                        if (tmpStudent.getID() == studentID)
                        {
                            battleStudent.Add(tmpStudent);
                            break;
                        }
                    }
                }

                List<Student> accept = new List<Student>();
                accept.Add(frmMain.getGame().getUser());

                string text = txtToken.Text.Replace(",", "");
                int token = Convert.ToInt32(text);
                
                if (lvwStuent.Items.Count == 1)
                {
                    battle = new PersonalBattle(battleStudent, accept, token, true);
                }
                else
                {
                    battle = new GroupBattle(battleStudent, accept, token, true);
                }

                frmMain.getGame().getUser().addToken(token * -1); // 추가
                frmMain.update();

                frmMain.getGame().getBattle().Add(battle);

                frmGamble.update();

                this.Close();
            }            
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (frmMain.getGame().getUser().getID() == Convert.ToInt32(txtStdID.Text))
            {
                MessageBox.Show("본인 검색불가");
            }
            else
            {
                bool isStudnt = false;

                foreach (ListViewItem row in this.lvwStuent.Items)
                {
                    int studentID = Convert.ToInt32(row.SubItems[0].Text);

                    if(studentID == Convert.ToInt32(txtStdID.Text))
                    {
                        isStudnt = true;
                        break;
                    }
                }

                if(!isStudnt)
                {
                    Student selStudent = null;

                    List<Student> student = frmMain.getGame().getStudent();
                    for (IEnumerator i = student.GetEnumerator(); i.MoveNext(); )
                    {
                        Student tmpStudent = (Student)i.Current;

                        if (tmpStudent.getID() == Convert.ToInt32(txtStdID.Text))
                        {
                            selStudent = tmpStudent;
                            break;
                        }
                    }

                    if (selStudent != null)
                    {
                        ListViewItem item = new ListViewItem(Convert.ToString(selStudent.getID()));
                        item.SubItems.Add(selStudent.getName());

                        lvwStuent.Items.Add(item);

                        this.Controls.Add(lvwStuent);
                    }
                    else
                    {
                        MessageBox.Show("존재하지 않는 학번");
                    }
                }
                else
                {
                    MessageBox.Show("이미 추가한 학생");
                }
            }

            txtStdID.Text = "";
            txtStdID.Focus();
        }

        private void txtStdID_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.btnSearch_Click(sender, e);
            }
        }

        private void txtStdID_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsDigit(e.KeyChar) || e.KeyChar == Convert.ToChar(Keys.Back)))
            {
                e.Handled = true;
            }
        }

        private void txtToken_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsDigit(e.KeyChar) || e.KeyChar == Convert.ToChar(Keys.Back)))
            {
                e.Handled = true;
            }
        }

        private void txtToken_TextChanged(object sender, EventArgs e)
        {
            if ((string.IsNullOrEmpty(txtToken.Text)))
            {
                txtToken.Text = "1";
            }

            string text = txtToken.Text.Replace(",", "");
            int value = Convert.ToInt32(text);

            int token = frmMain.getGame().getUser().getToken();

            if (value > 100000)
            {
                value = 100000;
                MessageBox.Show("100,000 포인트까지 가능");
            }
            else if (value > token)
            {
                value = token;
                MessageBox.Show("보유한 포인트까지 가능");
            }
            else if (value < 1)
            {
                value = 1;
                MessageBox.Show("1 포인트부터 가능");
            }

            txtToken.Text = String.Format("{0:###,0}", value);
            txtToken.SelectionStart = txtToken.TextLength;
        }
    }
}
