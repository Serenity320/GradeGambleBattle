﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GradeGambleBattle
{
    public partial class FormExam : Form
    {
        private FormMain frmMain;
        private FormGrade frmGrade;

        private Apply apply;
        private bool term;
        private int cnt;
        private int win;
        private int score;

        public FormExam(FormMain frmMain, FormGrade frmGrade, Apply apply, bool term)
        {
            InitializeComponent();

            // Initialize
            this.frmMain = frmMain;
            this.frmGrade = frmGrade;
            this.apply = apply;
            this.term = term;
            this.cnt = 0;
            this.win = 0;

            // Form
            if (!term) this.Text = "기말고사";

            // ListView
            lvwGrade.View = View.Details;
            lvwGrade.FullRowSelect = true;
            lvwGrade.GridLines = true;

            lvwGrade.Columns.Add("번호");
            lvwGrade.Columns.Add("학생");
            lvwGrade.Columns.Add("컴퓨터");
            lvwGrade.Columns.Add("결과");

            foreach (ColumnHeader column in lvwGrade.Columns)
            {
                column.Width = -2;
            }

            this.Controls.Add(lvwGrade);

            // Button
            btnEnd.Enabled = false;
        }

        public void exam(string user)
        {
            Random rnd = new Random();

            string[] type = { "가위", "바위", "보" };
            string computer = type[rnd.Next(0, 2)];

            string result = "";

            if (user.Equals("가위"))
            {
                if (computer.Equals("보")) result = "승";
                else if (computer.Equals("바위")) result = "패";
                else result = "무";
            }
            else if (user.Equals("바위"))
            {
                if (computer.Equals("가위")) result = "승";
                else if (computer.Equals("보")) result = "패";
                else result = "무";
            }
            else
            {
                if (computer.Equals("바위")) result = "승";
                else if (computer.Equals("가위")) result = "패";
                else result = "무";
            }

            ListViewItem item = new ListViewItem(Convert.ToString(cnt + 1));
            item.SubItems.Add(user);
            item.SubItems.Add(computer);
            item.SubItems.Add(result);

            lvwGrade.Items.Add(item);
            lvwGrade.Items[lvwGrade.Items.Count - 1].EnsureVisible();

            if (result.Equals("승")) win++;
            if (!result.Equals("무")) cnt++;

            if (cnt >= 10)
            {
                score = win * 10;

                lblGrade.Text = "점수 : " + Convert.ToString(score);

                btnGame1.Enabled = false;
                btnGame2.Enabled = false;
                btnGame3.Enabled = false;

                btnEnd.Enabled = true;
            }
        }
        private void btnEnd_Click(object sender, EventArgs e)
        {
            if (term)
            {
                apply.setMiddleScore(score);
            }
            else
            {
                apply.setFinalScore(score);
                apply.calGrade();

                if (frmMain.getGame().getUser().isEndFinalExam())
                {
                    frmMain.getGame().getUser().calGrade();
                }
            }

            frmGrade.update();

            this.Close();
        }

        private void btnGame1_Click(object sender, EventArgs e)
        {
            exam("가위");
        }

        private void btnGame2_Click(object sender, EventArgs e)
        {
            exam("바위");
        }

        private void btnGame3_Click(object sender, EventArgs e)
        {
            exam("보");
        }
    }
}
