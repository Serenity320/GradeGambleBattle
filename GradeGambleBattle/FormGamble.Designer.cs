﻿namespace GradeGambleBattle
{
    partial class FormGamble
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnBattleSearch = new System.Windows.Forms.Button();
            this.lvwGamble = new System.Windows.Forms.ListView();
            this.btnBattleAccept = new System.Windows.Forms.Button();
            this.btnReject = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnBattleSearch
            // 
            this.btnBattleSearch.Location = new System.Drawing.Point(408, 226);
            this.btnBattleSearch.Name = "btnBattleSearch";
            this.btnBattleSearch.Size = new System.Drawing.Size(80, 25);
            this.btnBattleSearch.TabIndex = 8;
            this.btnBattleSearch.Text = "배틀신청";
            this.btnBattleSearch.UseVisualStyleBackColor = true;
            this.btnBattleSearch.Click += new System.EventHandler(this.btnGamble_Click);
            // 
            // lvwGamble
            // 
            this.lvwGamble.Location = new System.Drawing.Point(0, 0);
            this.lvwGamble.Name = "lvwGamble";
            this.lvwGamble.Size = new System.Drawing.Size(500, 220);
            this.lvwGamble.TabIndex = 7;
            this.lvwGamble.UseCompatibleStateImageBehavior = false;
            // 
            // btnBattleAccept
            // 
            this.btnBattleAccept.Location = new System.Drawing.Point(12, 225);
            this.btnBattleAccept.Name = "btnBattleAccept";
            this.btnBattleAccept.Size = new System.Drawing.Size(80, 25);
            this.btnBattleAccept.TabIndex = 13;
            this.btnBattleAccept.Text = "배틀수락";
            this.btnBattleAccept.Click += new System.EventHandler(this.btnBattleAccept_Click);
            // 
            // btnReject
            // 
            this.btnReject.Location = new System.Drawing.Point(98, 225);
            this.btnReject.Name = "btnReject";
            this.btnReject.Size = new System.Drawing.Size(80, 25);
            this.btnReject.TabIndex = 12;
            this.btnReject.Text = "배틀거절";
            this.btnReject.UseVisualStyleBackColor = true;
            this.btnReject.Click += new System.EventHandler(this.btnReject_Click);
            // 
            // FormGamble
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(500, 251);
            this.Controls.Add(this.btnReject);
            this.Controls.Add(this.btnBattleAccept);
            this.Controls.Add(this.btnBattleSearch);
            this.Controls.Add(this.lvwGamble);
            this.Name = "FormGamble";
            this.Text = "FormGamble";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnBattleSearch;
        private System.Windows.Forms.ListView lvwGamble;
        private System.Windows.Forms.Button btnBattleAccept;
        private System.Windows.Forms.Button btnReject;
    }
}