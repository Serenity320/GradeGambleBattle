﻿using System;
using System.Collections; // IEnumerator
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GradeGambleBattle
{
    public partial class FormGrade : Form
    {
        private FormMain frmMain;

        public FormGrade(FormMain frmMain)
        {
            InitializeComponent();

            this.frmMain = frmMain;

            update();
        }
        public void update()
        {
            // ListView
            lvwGrade.FocusedItem = null;
            lvwGrade.Clear();

            lvwGrade.View = View.Details;
            lvwGrade.FullRowSelect = true;
            lvwGrade.GridLines = true;

            lvwGrade.Columns.Add("과목번호");
            lvwGrade.Columns.Add("교과목명");
            lvwGrade.Columns.Add("교수이름");
            lvwGrade.Columns.Add("학점");
            lvwGrade.Columns.Add("중간고사");
            lvwGrade.Columns.Add("기말고사");
            lvwGrade.Columns.Add("총점");

            List<Apply> apply = frmMain.getGame().getUser().getApply();

            for (IEnumerator i = apply.GetEnumerator(); i.MoveNext(); )
            {
                Apply tmpApply = (Apply)i.Current;
                Subject tmpSubjet = tmpApply.getSubject();

                ListViewItem item = new ListViewItem(Convert.ToString(tmpSubjet.getSubjectID()));
                item.SubItems.Add(tmpSubjet.getSubjectName());
                item.SubItems.Add(tmpSubjet.getProfessorName());
                item.SubItems.Add(Convert.ToString(tmpSubjet.getGrade()));

                int middleScore = tmpApply.getMiddleScore();
                if (middleScore < 0) item.SubItems.Add("-");
                else item.SubItems.Add(Convert.ToString(middleScore));

                int finalScore = tmpApply.getFinalScore();
                if (finalScore < 0) item.SubItems.Add("-");
                else item.SubItems.Add(Convert.ToString(finalScore));

                double grade = tmpApply.getGrade();
                if (grade < 0) item.SubItems.Add("-");
                else item.SubItems.Add(String.Format("{0:0.0}", grade));

                lvwGrade.Items.Add(item);
            }

            foreach (ColumnHeader column in lvwGrade.Columns)
            {
                column.Width = -2;
            }

            this.Controls.Add(lvwGrade);

            double stdGrade = frmMain.getGame().getUser().getGrade();

            if (stdGrade < 0)
            {
                lblGrade.Text = "총 학점 : 평가중";
            }
            else
            {
                lblGrade.Text = "총 학점 : " + String.Format("{0:0.00}", stdGrade);
            }
        }

        public Apply returnApply()
        {
            Apply selApply = null;

            int idx = lvwGrade.FocusedItem.Index;
            int subjectID = Convert.ToInt32(lvwGrade.Items[idx].SubItems[0].Text);

            List<Apply> apply = frmMain.getGame().getUser().getApply();

            for (IEnumerator i = apply.GetEnumerator(); i.MoveNext(); )
            {
                Apply tmpApply = (Apply)i.Current;

                if (tmpApply.getSubject().getSubjectID() == subjectID)
                {
                    selApply = tmpApply;
                    break;
                }
            }

            return selApply;
        }

        private void btnMiddleExam_Click(object sender, EventArgs e)
        {
            int count = lvwGrade.SelectedItems.Count;

            if (count > 0)
            {
                if(frmMain.getGame().getUser().calSubjectGrade() >= 18)
                {
                    Apply apply = returnApply();

                    if (apply.getMiddleScore() < 0)
                    {
                        apply.setMiddleScore(0);

                        FormExam frmExam = new FormExam(frmMain, this, apply, true);
                        frmExam.Show();
                    }
                    else
                    {
                        MessageBox.Show("시험완료 과목");
                    }
                }
                else
                {
                    MessageBox.Show("수강신청 완료 후 가능(18학점 이상)");
                }
            }

            update();
        }

        private void btnFinalExam_Click(object sender, EventArgs e)
        {
            int count = lvwGrade.SelectedItems.Count;

            if (count > 0)
            {
                if (!frmMain.getGame().getUser().isEndMiddelExam())
                {
                    MessageBox.Show("모든 중간고사 시험 후 가능");
                }
                else
                {
                    Apply apply = returnApply();

                    if (apply.getMiddleScore() < 0)
                    {
                        MessageBox.Show("중간고사 필수");
                    }
                    else if (apply.getFinalScore() < 0)
                    {
                        apply.setFinalScore(0);
                        apply.calGrade();

                        if (frmMain.getGame().getUser().isEndFinalExam())
                        {
                            frmMain.getGame().getUser().calGrade();
                        }

                        FormExam frmExam = new FormExam(frmMain, this, apply, false);
                        frmExam.Show();
                    }
                    else
                    {
                        MessageBox.Show("시험완료 과목");
                    }
                }
            }

            update();
        }
    }
}
