﻿namespace GradeGambleBattle
{
    partial class FormMain
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다.
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.강의목록ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.수강신청ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.성적조회ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.학점배틀ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pnlMain = new System.Windows.Forms.Panel();
            this.lblNum = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtNumber = new System.Windows.Forms.TextBox();
            this.txtMajor = new System.Windows.Forms.TextBox();
            this.txtName = new System.Windows.Forms.TextBox();
            this.txtMajor2 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtYears = new System.Windows.Forms.TextBox();
            this.txtToken = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.picStudent = new System.Windows.Forms.PictureBox();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picStudent)).BeginInit();
            this.SuspendLayout();
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.강의목록ToolStripMenuItem,
            this.수강신청ToolStripMenuItem,
            this.성적조회ToolStripMenuItem,
            this.학점배틀ToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(524, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // 강의목록ToolStripMenuItem
            // 
            this.강의목록ToolStripMenuItem.Name = "강의목록ToolStripMenuItem";
            this.강의목록ToolStripMenuItem.Size = new System.Drawing.Size(67, 20);
            this.강의목록ToolStripMenuItem.Text = "강의목록";
            this.강의목록ToolStripMenuItem.Click += new System.EventHandler(this.강의목록ToolStripMenuItem_Click);
            // 
            // 수강신청ToolStripMenuItem
            // 
            this.수강신청ToolStripMenuItem.Name = "수강신청ToolStripMenuItem";
            this.수강신청ToolStripMenuItem.Size = new System.Drawing.Size(67, 20);
            this.수강신청ToolStripMenuItem.Text = "수강신청";
            this.수강신청ToolStripMenuItem.Click += new System.EventHandler(this.수강신청ToolStripMenuItem_Click);
            // 
            // 성적조회ToolStripMenuItem
            // 
            this.성적조회ToolStripMenuItem.Name = "성적조회ToolStripMenuItem";
            this.성적조회ToolStripMenuItem.Size = new System.Drawing.Size(67, 20);
            this.성적조회ToolStripMenuItem.Text = "성적조회";
            this.성적조회ToolStripMenuItem.Click += new System.EventHandler(this.성적조회ToolStripMenuItem_Click);
            // 
            // 학점배틀ToolStripMenuItem
            // 
            this.학점배틀ToolStripMenuItem.Name = "학점배틀ToolStripMenuItem";
            this.학점배틀ToolStripMenuItem.Size = new System.Drawing.Size(67, 20);
            this.학점배틀ToolStripMenuItem.Text = "학점배틀";
            this.학점배틀ToolStripMenuItem.Click += new System.EventHandler(this.학점배틀ToolStripMenuItem_Click);
            // 
            // pnlMain
            // 
            this.pnlMain.Location = new System.Drawing.Point(12, 140);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Size = new System.Drawing.Size(500, 250);
            this.pnlMain.TabIndex = 12;
            // 
            // lblNum
            // 
            this.lblNum.AutoSize = true;
            this.lblNum.Location = new System.Drawing.Point(125, 50);
            this.lblNum.Name = "lblNum";
            this.lblNum.Size = new System.Drawing.Size(49, 12);
            this.lblNum.TabIndex = 2;
            this.lblNum.Text = "학     번";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(316, 50);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 12);
            this.label1.TabIndex = 4;
            this.label1.Text = "성     명";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(125, 80);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 12);
            this.label2.TabIndex = 6;
            this.label2.Text = "학부(과)";
            // 
            // txtNumber
            // 
            this.txtNumber.BackColor = System.Drawing.SystemColors.Window;
            this.txtNumber.Location = new System.Drawing.Point(182, 47);
            this.txtNumber.Name = "txtNumber";
            this.txtNumber.ReadOnly = true;
            this.txtNumber.Size = new System.Drawing.Size(120, 21);
            this.txtNumber.TabIndex = 3;
            // 
            // txtMajor
            // 
            this.txtMajor.BackColor = System.Drawing.SystemColors.Window;
            this.txtMajor.Location = new System.Drawing.Point(182, 77);
            this.txtMajor.Name = "txtMajor";
            this.txtMajor.ReadOnly = true;
            this.txtMajor.Size = new System.Drawing.Size(120, 21);
            this.txtMajor.TabIndex = 7;
            // 
            // txtName
            // 
            this.txtName.BackColor = System.Drawing.SystemColors.Window;
            this.txtName.Location = new System.Drawing.Point(373, 47);
            this.txtName.Name = "txtName";
            this.txtName.ReadOnly = true;
            this.txtName.Size = new System.Drawing.Size(120, 21);
            this.txtName.TabIndex = 5;
            // 
            // txtMajor2
            // 
            this.txtMajor2.BackColor = System.Drawing.SystemColors.Window;
            this.txtMajor2.Location = new System.Drawing.Point(373, 77);
            this.txtMajor2.Name = "txtMajor2";
            this.txtMajor2.ReadOnly = true;
            this.txtMajor2.Size = new System.Drawing.Size(120, 21);
            this.txtMajor2.TabIndex = 9;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(316, 80);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 12);
            this.label3.TabIndex = 8;
            this.label3.Text = "제1전공";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(125, 110);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(49, 12);
            this.label4.TabIndex = 10;
            this.label4.Text = "학     년";
            // 
            // txtYears
            // 
            this.txtYears.BackColor = System.Drawing.SystemColors.Window;
            this.txtYears.Location = new System.Drawing.Point(182, 107);
            this.txtYears.Name = "txtYears";
            this.txtYears.ReadOnly = true;
            this.txtYears.Size = new System.Drawing.Size(120, 21);
            this.txtYears.TabIndex = 11;
            // 
            // txtToken
            // 
            this.txtToken.BackColor = System.Drawing.SystemColors.Window;
            this.txtToken.Location = new System.Drawing.Point(373, 107);
            this.txtToken.Name = "txtToken";
            this.txtToken.ReadOnly = true;
            this.txtToken.Size = new System.Drawing.Size(120, 21);
            this.txtToken.TabIndex = 15;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(316, 110);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(49, 12);
            this.label5.TabIndex = 14;
            this.label5.Text = "포 인 트";
            // 
            // picStudent
            // 
            this.picStudent.BackColor = System.Drawing.Color.White;
            this.picStudent.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picStudent.Location = new System.Drawing.Point(30, 40);
            this.picStudent.Name = "picStudent";
            this.picStudent.Size = new System.Drawing.Size(80, 90);
            this.picStudent.TabIndex = 13;
            this.picStudent.TabStop = false;
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(524, 401);
            this.Controls.Add(this.txtToken);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.picStudent);
            this.Controls.Add(this.txtYears);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtMajor2);
            this.Controls.Add(this.txtName);
            this.Controls.Add(this.txtMajor);
            this.Controls.Add(this.txtNumber);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblNum);
            this.Controls.Add(this.pnlMain);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "FormMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Grade Gamble Battle ";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FormMain_FormClosed);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picStudent)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 수강신청ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 성적조회ToolStripMenuItem;
        private System.Windows.Forms.Panel pnlMain;
        private System.Windows.Forms.Label lblNum;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtNumber;
        private System.Windows.Forms.TextBox txtMajor;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.TextBox txtMajor2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtYears;
        private System.Windows.Forms.TextBox txtToken;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.PictureBox picStudent;
        private System.Windows.Forms.ToolStripMenuItem 강의목록ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 학점배틀ToolStripMenuItem;

    }
}

