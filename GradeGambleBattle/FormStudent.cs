﻿using System;
using System.Collections; // IEnumerator
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GradeGambleBattle
{
    public partial class FormStudent : Form
    {
        private FormMain frmMain;

        public FormStudent(FormMain frmMain)
        {
            InitializeComponent();

            this.frmMain = frmMain;

            update();
        }

        public void update()
        {
            // ListView
            lvwStudent.FocusedItem = null;
            lvwStudent.Clear();

            lvwStudent.View = View.Details;
            lvwStudent.FullRowSelect = true;
            lvwStudent.GridLines = true;

            lvwStudent.Columns.Add("과목번호");
            lvwStudent.Columns.Add("교과목명");
            lvwStudent.Columns.Add("교수이름");
            lvwStudent.Columns.Add("학점");

            List<Apply> apply = frmMain.getGame().getUser().getApply();

            for (IEnumerator i = apply.GetEnumerator(); i.MoveNext(); )
            {
                Apply tmpApply = (Apply)i.Current;
                Subject tmpSubject = tmpApply.getSubject();

                ListViewItem item = new ListViewItem(Convert.ToString(tmpSubject.getSubjectID()));
                item.SubItems.Add(tmpSubject.getSubjectName());
                item.SubItems.Add(tmpSubject.getProfessorName());
                item.SubItems.Add(Convert.ToString(tmpSubject.getGrade()));

                lvwStudent.Items.Add(item);
            }

            foreach (ColumnHeader column in lvwStudent.Columns)
            {
                column.Width = -2;
            }

            this.Controls.Add(lvwStudent);

            // Label
            lblGrade.Text = "수강학점 : " + Convert.ToString(frmMain.getGame().getUser().calSubjectGrade());
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            int count = lvwStudent.SelectedItems.Count;

            if (count > 0)
            {
                Apply selApply = null;

                int idx = lvwStudent.FocusedItem.Index;
                int subjectID = Convert.ToInt32(lvwStudent.Items[idx].SubItems[0].Text);

                List<Apply> apply = frmMain.getGame().getUser().getApply();

                for (IEnumerator i = apply.GetEnumerator(); i.MoveNext(); )
                {
                    Apply tmpApply = (Apply)i.Current;

                    if (tmpApply.getSubject().getSubjectID() == subjectID)
                    {
                        selApply = tmpApply;
                        break;
                    }
                }

                if (selApply == null)
                {
                    MessageBox.Show("수강취소 오류");
                }
                else
                {
                    if (frmMain.getGame().getUser().isStartMiddelExam())
                    {
                        MessageBox.Show("수강취소 기간 종료");
                    }
                    else
                    {
                        frmMain.getGame().getUser().cancelSubject(selApply);
                        selApply.delApply();

                        MessageBox.Show("수강취소 완료");
                    }
                }
            }

            update();
        }
    }
}
