﻿namespace GradeGambleBattle
{
    partial class FormSubject
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lvwSubject = new System.Windows.Forms.ListView();
            this.btnSubject = new System.Windows.Forms.Button();
            this.lblGrade = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lvwSubject
            // 
            this.lvwSubject.Location = new System.Drawing.Point(0, 0);
            this.lvwSubject.Name = "lvwSubject";
            this.lvwSubject.Size = new System.Drawing.Size(500, 220);
            this.lvwSubject.TabIndex = 0;
            this.lvwSubject.UseCompatibleStateImageBehavior = false;
            // 
            // btnSubject
            // 
            this.btnSubject.Location = new System.Drawing.Point(408, 226);
            this.btnSubject.Name = "btnSubject";
            this.btnSubject.Size = new System.Drawing.Size(80, 25);
            this.btnSubject.TabIndex = 3;
            this.btnSubject.Text = "수강신청";
            this.btnSubject.UseVisualStyleBackColor = true;
            this.btnSubject.Click += new System.EventHandler(this.btnSubject_Click);
            // 
            // lblGrade
            // 
            this.lblGrade.AutoSize = true;
            this.lblGrade.Location = new System.Drawing.Point(12, 232);
            this.lblGrade.Name = "lblGrade";
            this.lblGrade.Size = new System.Drawing.Size(71, 12);
            this.lblGrade.TabIndex = 6;
            this.lblGrade.Text = "수강학점 : 0";
            // 
            // FormSubject
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(500, 251);
            this.Controls.Add(this.lblGrade);
            this.Controls.Add(this.btnSubject);
            this.Controls.Add(this.lvwSubject);
            this.Name = "FormSubject";
            this.Text = "FormSubject";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView lvwSubject;
        private System.Windows.Forms.Button btnSubject;
        private System.Windows.Forms.Label lblGrade;


    }
}