﻿using System;
using System.Collections.Generic;
using System.Data; // DataTable
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GradeGambleBattle
{
    class StudentDAO
    {
        private DBConnector dbConnector;

        public StudentDAO()
        {
            this.dbConnector = new DBConnector();
        }

        public int checkLogin(string stdID, string stdPW)
        {
            DataTable dataTable;

            string query = "select count(*) as cnt from student where stdID = '" + stdID + "' and stdPW = password('" + stdPW + "')";
            dataTable = dbConnector.selectQuery(query);

            dataTable.Columns[0].ColumnName = "cnt";

            int cnt = Convert.ToInt32(dataTable.Rows[0]["cnt"]);

            return cnt;
        }

        public void setGrade(int id, double grade)
        {
            string query = "update student set stdGrade = '" + grade + "' where stdID = '" + id + "'";
            dbConnector.sendQuery(query);
        }

        public void setToken(int id, int token)
        {
            string query = "update student set stdToken = '" + token + "' where stdID = '" + id + "'";
            dbConnector.sendQuery(query);
        }
    }
}
