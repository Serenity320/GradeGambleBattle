﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GradeGambleBattle
{
    class SubjectDAO
    {
        private DBConnector dbConnector;

        public SubjectDAO()
        {
            this.dbConnector = new DBConnector();
        }


        public void incPresentNum(int sbjID)
        {
            string query = "update subject set sbjPresentNum = sbjPresentNum + 1 where sbjID = '" + sbjID + "'";
            dbConnector.sendQuery(query);
        }

        public void decPresentNum(int sbjID)
        {
            string query = "update subject set sbjPresentNum = sbjPresentNum - 1 where sbjID = '" + sbjID + "'";
            dbConnector.sendQuery(query);
        }
    }
}
